# 実用的コンテナ構築とデプロイ

# 1コンテナに1つの関心事

1コンテナ=1プロセスの考え方でアプリケーションを構築していくのには限界がある。

Dockerは1コンテナに1つの関心事という考えを推奨している

大切なことは、それぞれのコンテナが担うべき役割を適切に定め、かつそれがレプリカとして複製された場合でも副作用なくスタックとして正しく動作できるう状態になるか？という考えに基づいて設計すると良い

# コンテナのポータビリティ

Dockerのポータビリティは完全ではなく、いくつか課題があるので紹介

## Kernel・アーキテクチャの違い

Dockerのコンテナ型仮想化技術では、ホストOSとカーネルのリソースを共有している。

これは事実上、Dockerコンテナが実行できるホストは、ある特定のCPUアーキテクチャーやOSの前提の上に成立しているということを意味している。

アーキテクチャーに縛られるということ。

## ライブラリDynamic Linkの課題

ネイティブライブラリをダイナミックリンクするようなケースでもポータビリティは損なわれる。

スタティックリンクではアプリケーションが必要なライブラリをアプリケーションに内包する形式であり、アプリケーションのサイズは膨らむ傾向にあるがポータビリティ性には優れている。

一方ダイナミックリンクではアプリケーションを実行する際にリンクするのでサイズは小さくなるが実行ホストに用意すべきライブラリを管理するのが大変

Dockerコンテナ上での実行を想定したアプリケーションを作るにはネイティブライブラリを極力スタティックリンクしてビルドすることが第一

# Dockerフレンドリーなアプリケーション

**アプリケーションの挙動を環境変数で制御**

- 実行時引数
- 設定ファイル
- アプリケーションの挙動を環境変数で制御
- 設定ファイルに環境変数を埋め込む

**実行時引数として値を渡す**

- ○外部からの値の注入を果たせる
- △実行時引数が増えるとアプリケーション側での引数から任意の変数へのマッピングの処理が増えたり、CMDやENTRYPOINTがより煩雑になりがちで管理が難しくなっていく側面がある。

**設定ファイルとして渡す**

実行するアプリケーションにdevelopmentやproductionといった環境名を与えることで利用する設定ファイルを切り替える手法

- ○設定ファイルを利用するのであれば、コンテナの外から実行時に渡すことができれば環境が増えても新たにDockerイメージをビルドせずに済む
- △ホストに配置した環境別設定ファイルをコンテナにマウントするのも一つの手だけど、ホストに依存してしまうため管理・運用は難しくなる

**アプリケーションの挙動を環境変数で制御**

環境変数の管理はアプリケーションとは別のリポジトリで管理するのが一般

composeであれば、docker-compose.ymlのenv属性に列挙

各環境で利用する環境変数を定義したファイルを集約したリポジトリを作って管理するのが良い

**設定ファイルに環境変数を埋め込む**

設定ファイルのテンプレートに環境変数を埋め込む

# 永続化データの取扱

Docker コンテナを実行中に書き込まれたファイルはホスト側にファイル・ディレクトリをマウントしない限りコンテナを破棄したタイミングでディスクから消去される。

コンテナの中で実行されているアプリケーションが変更したファイルやディレクトリを利用する。

すなわち状態を持つステートフルな性質を持つのであれば一度コンテナを破棄してしまうと同じ状態を持つコンテナを再現することは容易ではない。

ステートフルな性質を持つアプリケーションをコンテナで運用するには新しいバージョンのコンテナがデプロイされても以前のバージョンのコンテナで利用していたファイル・ディレクトリをそのまま継続して利用できることが求められる。

こういったケースではData Volumeが利用される。

**Data Volume**

Docker コンテナ内のディレクトリをディスクに永続化するための仕組み

- 各コンテナとホストで永続化データを共有
    - イメージを更新して新しいコンテナを作成しても同じData Volumeを利用し続けることができる
- Data Volumeコンテナという永続化データ用のコンテナを起動する方法もある

Data Volume作成

```docker
docker container run [options] -v ホスト側ディレクトリ:コンテナ側ディレクトリ リポジトリ名[:タグ] [コマンド] [コマンド引数]              
```

ex)コンテナの中で生成したファイルをホストで参照するケース

```docker
docker container run -v ${PWD}:/workspace gihyodocker/imagemagick:latest convert -size 100×100 xc:#000000 /workspace/gihyo.jpg
```

convertから後ろがコンテナに渡すアプリケーションの引数でこれはimagemagickに/workspace/gihyo.jpgという100×100の黒い画像ファイルを生成させるコマンドです。

-vオプションでData Volumeを設定していて、コンテナ内の/workspaceディレクトリは環境変数${PWD}で表現されるディレクトリ(つまりカレンドディレクトリ)にマウントされる。

このコンテナはimagemagickのコマンドを実行するためのコンテナのため、画像生成処理が完了し次第停止する。

Docker Volumeは共有の仕組みであるため、ホスト側で編集したファイルをData Volumeを通じてイメージ更新することなくコンテナに共有することも可能

**Data Volume コンテナ**

データを持つだけのコンテナで、上記コンテナホスト間のデータ共有と異なり、こちらはコンテナ間でデータ共有

Dockerコンテナは破棄されない限り、その内容がディスクに保持される。

その特徴を活かし、ディスクに保持されたコンテナが持つ永続化データの一部をVolumeとして別のコンテナに共有できるようにしたもの。

Data Volumeコンテナによって共有されるディレクトリも、ホスト側のストレージに存在するという点ではData Volumeと同じ。

Data VolumeコンテナのVolumeはDockerの管理領域であるホスト側の/var/lib/docker/volumes/以下に配置されている。

Data Volumeコンテナ方式はDockerの管理下にあるディレクトリにのみ影響を与える。ホストコンテナ間Data Volume 方式に比べるとコンテナに与える影響を最小限に抑えられる。

コンテナ内のアプリケーションとデータの密結合が緩和されることでアプリケーションコンテナとData Volumeコンテナの付け替えや移行がスムーズ

![スクリーンショット 2023-04-19 22.26.19.png](assets/docker-swarm1.png)

# コンテナ配置戦略

**Docker Swarm**

複数のDockerホストを束ねてクラスタ化するためのツールで、コンテナオーケストレーションシステムの一つ

| 名称 | 役割 | 対応コマンド |
| --- | --- | --- |
| Compose | 複数のコンテナを使うDockerアプリの管理(主にシングルホスト) | docker compose |
| Swarm | クラスタの構築や管理を担う(主にマルチホスト) | docker swarm |
| Service | Swarm前提、クラスタ内のService(1つ以上のコンテナの集まり)を管理 | docker service |
| Stack | Swarm前提、複数のServiceをまとめたアプリ全体の管理 | docker stack |

**複数のDockerホストを用意し、Swarmクラスタを管理する**

docker in docker

Dockerホストとして機能するDockerコンテナを複数個立てる

![スクリーンショット 2023-04-20 8.54.43.png](assets/docker-swarm2.png)

- registryコンテナ
    - manager, workerから参照される
    - 外側のDockerに保持されているイメージを一度registryコンテナにpushしておき、managerとworkerのdindコンテナはregistryコンテナからDockerイメージを取得している
    - registryコンテナのデータは永続化のためホストにマウントしておく
- managerコンテナ
    - Swarmクラスタ全体を制御する役割
        - 複数実行されているWorkerと呼ばれるDockerホスト(ここではdindコンテナ)へServiceが保持するコンテナを適切に配置
        

```docker
version: "3"
services: 
  registry:
    container_name: registry
    image: registry:2.6
    ports:
      - 5000:5000
    volumes:
      - "./registry-data:/var/lib/registry"

  manager:
    container_name: manager
    image: docker:20.10.23-dind
    privileged: true
    tty: true
    ports:
      - 8000:80
      - 9000:9000
    depends_on:
      - registry
    expose:
      - 3375
    command: "--insecure-registry registry:5000"
    volumes:
      - "./stack:/stack"

  worker01:
    container_name: worker01
    image: docker:20.10.23-dind
    privileged: true
    tty: true
    depends_on:
      - manager
      - registry
    expose:
      - 7946
      - 7946/udp
      - 4789/udp
    command: "--insecure-registry registry:5000"

  worker02:
    container_name: worker02
    image: docker:20.10.23-dind
    privileged: true
    tty: true
    depends_on:
      - manager
      - registry
    expose:
      - 7946
      - 7946/udp
      - 4789/udp
    command: "--insecure-registry registry:5000"

  worker03:
    container_name: worker03
    image: docker:20.10.23-dind
    privileged: true
    tty: true
    depends_on:
      - manager
      - registry
    expose:
      - 7946
      - 7946/udp
      - 4789/udp
    command: "--insecure-registry registry:5000"

```

```docker
# dindコンテナを複数実行
docker compose up -d

# 5つのコンテナが正常に起動していることを確認
docker container ls

# ホストからmanagerコンテナに対してdocker swarm initを実行してSwarmのmanagerに設定
docker container exec -it manager docker swarm init
# 成功するとそのDockerホストはmanagerとしてマークされ、Swarmモードが有効になる
# ここからは複数のworkerコンテナを登録してクラスタ形成する
# docker swarm init実行時にJOINトークンが発行され標準出力に表示される。
# dockerホストをSwarmクラスタのworkerとして登録するにはこのJOINトークンが必要になる。

# JOINトークンを利用して3つのノードをSwarmクラスタにworkerとして登録
docker container exec -it docker swarm join \ --token ~~

# workerコンテナからmanagerコンテナはmanagerで名前解決可能なのでmanager:2377でOK
docker container exec -it worker01 docker swarm join --token your_token manager:2377

# $ This is joined a swarm as a worker.

# worker02, 03も同様

# swarmクラスタの状態になったかどうか確認。manager含め4つのnodeが存在すればOK
docker container exec -it manager docker node ls

# 外側のDockerでビルドしたイメージはレジストリ経由で取得する必要があるのでregistryにpush
# まずはタグ付け
docker image tag example/echo:latest localhost:5000/example/echo:latest

# タグを指定してpush
# docker image push [push先のレジストリのホスト/]リポジトリ名[:タグ名]
docker image push localhost:5000/example/echo:latest
```

**Service**

アプリケーションを構成する一部のコンテナを制御するための単位

コンテナ利用のアプリケーションのスケールアウトを容易にする

serviceを利用しない場合は、それぞれのノードで必要なコンテナ数だけdocker container run を繰り返すことになり手間

試しにserviceを作成

```docker
docker container exec -it manager \
docker service create --replicas 1 --publish 8000:8000 --name echo registry:5000/example/echo:latest

# service一覧の確認
docker container exec -it manager docker service ls

# 該当サービスのコンテナ数を増減させる
# 6個のechoコンテナが実行される
# Swarmクラスタのノードに分散して配置される
docker container exec -it manager docker service scale echo=6

# serviceの削除
docker container exec -it manager docker service rm echo
```

**Stack**

複数のServiceをグルーピングした単位であり、アプリケーションの全体の構成を定義している。

Serviceは1つのアプリケーションイメージしか扱うことができないが、複数のServiceが強調して動作することで成立アプリケーションもある。これを解決するのが上位概念のStack

StackによってデプロイされるService群はoverlayネットワークに所属している。

overlayネットワークとは複数のDockerホストにデプロイされているコンテナ群を同じネットワークに配置させることができる技術

overlayネットワークにより、Dockerホスト間を超えたコンテナ通信が可能となる


**overlayネットワークを構築**
```
docker container exec -it manager \
docker network create --driver=overlay --attachable todoapp
```