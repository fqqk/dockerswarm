# mac Monterey AirPlayのレシーバーがport5000でリッスンするようになっているため解除

mac MontereyにてAirPlayという機能が搭載され、それがport5000でリッスンするようになっていたため以下エラーが起きていた。
```
Error response from daemon: Ports are not available: exposing port TCP 0.0.0.0:5000 -> 0.0.0.0:0: listen tcp 0.0.0.0:5000: bind: address already in use
```

なのでairplayをOFFにした。(使ったこと無いし、まあいっか。)


もしくは、

```
  registry:
    container_name: registry
    image: registry:2.6
    ports:
      - 5001:5000
```
というふうにポートフォワーディングを変更してあげる。

こうすることで、localhostの5001番ポートがdocker内のポートをリッスンしてくれるようになる
0.0.0.0:5001->5000/tcp

# registryはマウントされるけど、それ以外のmananerとworkerがマウントされない問題
以下エラーログ

```
Error starting daemon: Devices cgroup isn't mounted
```

docker:18.05.0-ce-dindだと、バージョンが古いらしい。
バージョンを上げることで解消した事象を見つけたのでdocker:20.10.23-dindを使用することで解決
